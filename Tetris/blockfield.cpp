#include "blockfield.h"

BlockField::BlockField(unsigned int xFields, unsigned int yFields)
{
    for(unsigned int i = 0; i < yFields; i++){
        blockrow row(xFields);
        for(unsigned int j = 0; j < xFields; j++){
            Field temp(0, QColor(0,0,0,0));
            row[j] = temp;
        }
        field.push_back(row);
    }
    curX = xFields/2;
    curY = 0;
    compareField = field;
    rowCount = 0;
}

blockMatr* BlockField::update(){
    field = compareField;
    for(unsigned int i = 0; i < currBlock.size(); i++){
        for(unsigned int j = 0; j < currBlock[0].size(); j++){
            unsigned int tP = j-currBlock[i].size()/2;
            if(curY + i < field.size() && currBlock[i][j] + field[curY+i][curX+tP].set < 2){
                field[curY+i][curX+tP].color = currBlock[i][j] == 0 ? field[curY+i][curX+tP].color : currCol;
                field[curY+i][curX+tP].set += currBlock[i][j];
            }
            else{
                compareField = lastState;
                setNewBlock();
                checkRows();
                return &lastState;
            }
        }
    }
    lastState = field;
    return &field;
}

void BlockField::checkRows(){
    int rowsCaught = 0;
    for(unsigned int i = 0; i < compareField.size(); i++ ){
        bool rowFlag = true;
        for(unsigned int j = 0; j < compareField[i].size(); j++){
            if(compareField[i][j].set < 1 )
                rowFlag = false;
        }
        if(rowFlag){
            eliminateRow(i);
            ++rowsCaught;
        }
    }
    rowCount = rowsCaught;
}

void BlockField::eliminateRow(const int index){
    blockMatr newField;
    blockrow newEmptyRow;
    for(int i = 0; i < compareField[0].size(); i++){
        Field temp(0, QColor(0,0,0,0));
        newEmptyRow.push_back(temp);
    }
    newField.push_back(newEmptyRow);
    for(int i = 0; i < compareField.size(); i++){
        if(i!=index)
            newField.push_back(compareField[i]);
    }
    compareField = newField;

}

void BlockField::rotate(){
    matr temp = Blocks::rotateBlock(currBlock);

    for(unsigned int i = 0; i < temp.size(); i++){
        for(unsigned j = 0; j < temp[i].size(); j++){
            int tP = j-temp[0].size()/2;
            if( (curX+(temp[i].size()-1-temp[i].size()/2) > compareField[0].size() ) ||
                (curX-temp[i].size()/2 < 0) ||
                (curX+tP >= compareField[i].size() || curY+i >= compareField.size() ) ||
                (temp[i][j]+compareField[curY+i][curX+tP].set > 1)   )
                return;
        }
    }
    currBlock = temp;
}

void BlockField::setNewDir(const int dir){

    if( (curX - currBlock[0].size()/2 <= 0 && dir < 0) ||
        (curX + (currBlock[0].size()-currBlock[0].size()/2) >= field[0].size() && dir > 0 )   ){
        return;
    }
    for(unsigned int i = 0; i < currBlock.size(); i++){
           if( (currBlock[i][currBlock[0].size()-1] + field[curY+i][curX+(currBlock[0].size()-currBlock[0].size()/2)].set > 1 && dir > 0 ) ||
               (curX - currBlock[0].size()/2 > 0 && currBlock[i][0] + field[curY+i][curX-(1 + currBlock[0].size()/2)].set > 1 && dir < 0 )){
               return;
            }
    }
    curX += dir;

}

void BlockField::setNewBlock(){
    currBlock = nextBlock;
    nextBlock = Blocks::getRndmBlock();
    currCol = nextCol;
    nextCol = QColor(rand()%256, rand()%256, rand()%256, 255);
    curX = field[0].size()/2;
    curY = 0;
    //field = compareField;
}

void BlockField::setDown(){
    ++curY;
}

void BlockField::init(){
    nextBlock = Blocks::getRndmBlock();
    nextCol = QColor(rand()%256, rand()%256, rand()%256, 255);
    setNewBlock();
}
