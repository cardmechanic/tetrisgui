#include "gamewidget.h"
#include "ui_gamewidget.h"

GameWidget::GameWidget(QWidget *parent) : QWidget(parent), area(10,20),  ui(new Ui::GameWidget)
{
   ui->setupUi(this);

   QObject::connect( & timer, SIGNAL( timeout( ) ), this, SLOT( slotTimerEvent( ) ) );

   area.init();
   running = false;
   timeCount = 0;
   speed = 0;
   points = 0;

}

void GameWidget::startGame(const unsigned int lvl){
    setFocus();
    size().setWidth(size().height()/2);
    speed = lvl;
    running = true;
    timer.setInterval( static_cast< int >( 1000. / 60. ) );
    timer.start( );
    update();
}

void GameWidget::paintEvent(QPaintEvent * p_paintEvent){

    if(!running)
        return;

    QPainter painter( this );
    painter.fillRect(p_paintEvent->rect(), QColor(100, 100, 100, 255));
    blockMatr arena = *area.update();

    double xUnits = size().width()/double(arena[0].size() );
    double yUnits = size().height()/double(arena.size() );

    for(unsigned int i = 0; i < arena.size(); i++){
        for(unsigned int j = 0; j < arena[i].size(); j++){
            if(arena[i][j].set > 0){
                painter.setBrush(arena[i][j].color);
                QRectF rectangle(j*xUnits, i*yUnits,xUnits,yUnits);
                painter.drawRect(rectangle);
            }
        }

    }
    points += area.rowCount == 0 ? 0 : pow(area.rowCount, speed);
    emit nextBlock(area.nextBlock, area.nextCol, points);
    area.rowCount = 0;
}

void GameWidget::slotTimerEvent(){
    if(timeCount > 100-(speed*10)){
        timeCount = 0;
        area.setDown();
        update();
        return;
    }
    ++timeCount;
}

void GameWidget::keyReleaseEvent( QKeyEvent * p_keyEvent)
{

    if( p_keyEvent->key( ) == Qt::Key_Right ) {
        area.setNewDir(1);
        update();
    }
    if( p_keyEvent->key( ) == Qt::Key_Left ) {
        area.setNewDir(-1);
        update();
    }
    if( p_keyEvent->key( ) == Qt::Key_Down ) {
        area.setDown();
        update();
    }
    if( p_keyEvent->key( ) == Qt::Key_Space ) {
        area.rotate();
        update();
    }
}
