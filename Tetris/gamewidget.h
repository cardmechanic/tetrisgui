#ifndef GAMEWIDGET_H
#define GAMEWIDGET_H

#include "blockfield.h"
#include <QWidget>
#include <QTimer>
#include <QTime>
#include <QPainter>
#include <QPaintEvent>
#include <iostream>
#include <math.h>

namespace Ui {
class GameWidget;
}

class GameWidget : public QWidget
{
    Q_OBJECT
public:
    QTimer timer;
    QTime time;
    QPainter painter;
    BlockField area;

    bool running;
    unsigned int timeCount;
    unsigned int speed;
    unsigned int points;

    explicit GameWidget(QWidget *parent = nullptr);
    void paintEvent(QPaintEvent * p_paintEvent);
    void keyReleaseEvent( QKeyEvent * p_keyEvent );

signals:
    void nextBlock(const matr,const QColor&, const unsigned int);

public slots:
    void startGame(const unsigned int);
    void slotTimerEvent();

private:
    Ui::GameWidget *ui;
};

#endif // GAMEWIDGET_H
