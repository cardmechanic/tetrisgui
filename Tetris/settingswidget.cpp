#include "settingswidget.h"
#include "ui_settingswidget.h"

SettingsWidget::SettingsWidget(QWidget *parent) : QWidget(parent), ui(new Ui::SettingsWidget)
{
   ui->setupUi(this);

   QObject::connect(ui->startButton, SIGNAL(released()), this, SLOT(initGame() ) );
}

SettingsWidget::~SettingsWidget(){
    delete ui;
}


void SettingsWidget::initGame(){
    int* dats[3];//for later implemenations
    int level = ui->speed->value();
    int diff = ui->difficult->value(); //for later implemenations
    int earth =ui->checkBox->isChecked() ? 0 : 1; //for later implemenations
    emit gameData(level);
}
