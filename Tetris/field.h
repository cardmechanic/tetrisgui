#ifndef FIELD_H
#define FIELD_H

#include <QColor>

class Field
{
public:
    int set;
    QColor color;
    Field(int setInit, QColor col);
    Field();
};

#endif // FIELD_H
