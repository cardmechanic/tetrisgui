#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ui_settingswidget.h"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	 QObject::connect(ui->settingsWidget, SIGNAL(gameData(const unsigned int)), ui->gameWidget,
																	SLOT(startGame(const unsigned int) ) );
	 QObject::connect(ui->gameWidget, SIGNAL(nextBlock(const matr,const QColor&, const unsigned int)), ui->statsWidget,
																	SLOT(recieveNext(const matr,const QColor&, const unsigned int) ) );
}

MainWindow::~MainWindow()
{
	delete ui;
}
