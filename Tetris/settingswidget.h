#ifndef SETTINGSWIDGET_H
#define SETTINGSWIDGET_H

#include <QWidget>
#include "gamewidget.h"

namespace Ui {
class SettingsWidget;
class GameWidget;
}

class SettingsWidget : public QWidget
{
    Q_OBJECT
public:
    Ui::SettingsWidget *ui;

    explicit SettingsWidget(QWidget *parent = nullptr);
    ~SettingsWidget();

signals:
    void gameData(const unsigned int);

public slots:
    void initGame();

};

#endif // SETTINGSWIDGET_H
