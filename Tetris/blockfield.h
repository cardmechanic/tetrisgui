#ifndef BLOCKFIELD_H
#define BLOCKFIELD_H

#include "globals.h"
#include "blocks.h"
#include "field.h"
#include <iostream>

class BlockField
{
public:
    unsigned int curX;
    unsigned int curY;

    blockMatr field;
    blockMatr compareField;
    blockMatr lastState;

    matr currBlock;
    matr nextBlock;
    QColor currCol;
    QColor nextCol;

    unsigned int rowCount;
    BlockField(unsigned int xFields, unsigned int yFields);
    blockMatr* update();
    void init();
    void rotate();
    void setNewBlock();
    void checkRows();
    void eliminateRow(const int index);
    void setNewDir(const int dir);
    void setDown();
};

#endif // BLOCKFIELD_H
