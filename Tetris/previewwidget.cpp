#include "previewwidget.h"
#include "ui_previewwidget.h"

PreviewWidget::PreviewWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::PreviewWidget)
{
	ui->setupUi(this);
}

PreviewWidget::~PreviewWidget()
{
	delete ui;
}

void PreviewWidget::recieveNext(const matr& block, const QColor &color){
   nextBlock = block;
   nextCol = color;
   set = true;
   update();
}
void PreviewWidget::paintEvent(QPaintEvent * p_paintEvent){
	if(!set)
		return;
	QPainter painter(this );
	painter.fillRect(p_paintEvent->rect(), QColor(100, 100, 100, 255));
	double xUnits = (size().width()/4)/2;
	double yUnits = (size().width()/4)/2;
	for(unsigned int i = 0; i < nextBlock.size(); i++){
		for(unsigned int j = 0; j <nextBlock[i].size(); j++){
			if(nextBlock[i][j] > 0){
				painter.setBrush(nextCol);
				QRectF rectangle(size().width()/4+j*xUnits, size().height()/4+i*yUnits,xUnits,yUnits);
				painter.drawRect(rectangle);
			}
		}
	}
}
