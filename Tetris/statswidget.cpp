#include "statswidget.h"
#include "ui_statswidget.h"

StatsWidget::StatsWidget(QWidget *parent) : QWidget(parent), ui(new Ui::StatsWidget)
{
   ui->setupUi(this);
}

void StatsWidget::recieveNext(const matr& block,const QColor& color, const unsigned int points){
  ui->previewWidget->recieveNext(block, color);
  QFont f( "Arial", 15, QFont::Bold);
  ui->label->setFont( f);
  QString out = QString::number(points);
  ui->label->setText(out);
}


