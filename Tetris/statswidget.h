#ifndef STATSWIDGET_H
#define STATSWIDGET_H

#include <QWidget>
#include <QColor>
#include <QPainter>
#include <QPaintEvent>
#include "globals.h"

namespace Ui {
class StatsWidget;
}
class StatsWidget : public QWidget
{
    Q_OBJECT
public:
    Ui::StatsWidget *ui;
    explicit StatsWidget(QWidget *parent = nullptr);

signals:

public slots:
       void recieveNext(const matr &, const QColor &, const unsigned int);
private:

};

#endif // STATSWIDGET_H
