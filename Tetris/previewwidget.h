#ifndef PREVIEWWIDGET_H
#define PREVIEWWIDGET_H

#include <QWidget>
#include <QColor>
#include <QPainter>
#include <QPaintEvent>
#include "globals.h"

namespace Ui {
class PreviewWidget;
}

class PreviewWidget : public QWidget
{
	Q_OBJECT

public:
	matr nextBlock;
	QColor nextCol;
	QPainter painter;
	bool set;

	explicit PreviewWidget(QWidget *parent = nullptr);
	~PreviewWidget();
	void paintEvent(QPaintEvent * p_paintEvent);
	void recieveNext(const matr &, const QColor &);

signals:

public slots:

private:
	Ui::PreviewWidget *ui;
};

#endif // PREVIEWWIDGET_H
