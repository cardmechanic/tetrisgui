#ifndef GLOBALS_H
#define GLOBALS_H

#include <vector>

class Field;

typedef std::vector<std::vector<int> > matr;
typedef std::vector<int> arr;
typedef std::vector<std::vector<Field> > blockMatr;
typedef std::vector<Field> blockrow;

#endif // GLOBALS_H
