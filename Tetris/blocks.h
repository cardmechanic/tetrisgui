#ifndef BLOCKS_H
#define BLOCKS_H

#include "globals.h"
#include <cstdlib>

namespace {
class Blocks
{
public:
    static matr getRndmBlock();
    static matr rotateBlock(const matr& block);

private:
     static std::vector<matr> blocks;

};

std::vector<matr> Blocks::blocks = {
    {
       {1,1,1,1}
    },

    {
        {1,1},
        {1,1}
    },

    {
        {1,0,0},
        {1,1,1}
    },

    {
        {0,1,0},
        {1,1,1}
    },

    {
        {0,0,1},
        {1,1,1}
    },

    {
        {1,1,0},
        {0,1,1}
    },
    {
        {0,1,1},
        {1,1,0}
    },

};
matr Blocks::getRndmBlock(){
    return blocks[rand()%blocks.size()];
}
matr Blocks::rotateBlock(const matr& block){
    matr rotated;
    for(int i = 0 ; i < block[0].size() ; i++){
        arr temp;
        for(int j = block.size()-1; j >= 0 ; j--){
            temp.push_back(block[j][i]);
        }
        rotated.push_back(temp);
    }

    return rotated;
}

}
#endif // BLOCKS_H
